### What is mydebiankernelgrsec? ###

It is a simple Bash Script to let user download from GrSecurity.net patches and vanilla kernel tarball from from Kernel.org, and compile them using the Debian way.

### What is done underneath? ###

Basically all steps described in http://pastebin.com/KtefQuVg

### What do I need before run this script? ###

```
#!shell
$ sudo apt-get install libncurses5-dev
$ sudo apt-get install kernel-package
$ sudo apt-get install fakeroot build-essential devscripts
```
### How to run? ###

Create a simple folder in your home directory and run

-k GrSecurity patch version available

-c Any configuration file you want to import to your new kernel build

-t Download Testing Patch according to GrSecurity new policy

```
#!shell
$ mydebiankernelgrsec.sh -t -k 4.2.6 -c /boot/config-3.19.8-kaiux

```

Or

```
#!shell
$ mydebiankernelgrsec.sh -k 3.2.68 -c /boot/config-3.19.8-kaiux
```


### What are the results? ###

```
#!shell
$ ls *.deb
linux-firmware-image_3.2.68-grsec-1_amd64.deb        linux-image-3.2.68-grsec_3.2.68-grsec-1_amd64.deb
linux-headers-3.2.68-grsec_3.2.68-grsec-1_amd64.deb  linux-libc-dev_3.2.68-grsec-1_amd64.deb
```
or

```
#!shell
linux-firmware-image-4.2.6-grsec_4.2.6-grsec-1_amd64.deb
linux-headers-4.2.6-grsec_4.2.6-grsec-1_amd64.deb
linux-image-4.2.6-grsec_4.2.6-grsec-1_amd64.deb
linux-image-4.2.6-grsec-dbg_4.2.6-grsec-1_amd64.deb
linux-libc-dev_4.2.6-grsec-1_amd64.deb
```

After that, you can install Linux Kernel with GrSecurity or distribute among your servers


#!/usr/bin/env bash
# Copyright 2016, 2015 - Kaio Rafael @kaiux
# Script to Build Vanila Kernel with GrSecurity follwing Debian Way

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

### Variables
KERNEL_SITE="https://www.kernel.org"
KERNEL_SITE_FEEDS="${KERNEL_SITE}/feeds/kdist.xml"
GRSEC_SITE_FEEDS="http://grsecurity.net/stable_rss.php"
GRSEC_TESTING_FEEDS="http://grsecurity.net/testing_rss.php"

declare -a TO_DOWNLOAD

#### Utils
XZ_UNTAR="xz -d"
UNTAR="tar -xf"
#XZ_UNTAR="tar -xJf"
GREG_KEY="647F28654894E3BD457199BE38DBBDC86092693E" # for Kernel.com 
GRSC_KEY="DE9452CE46F42094907F108B44D1C0F82525FE49" # GrSecurity Key

KERNEL_STABLE_VERSION="000" #simple check to avoid error
CONFIG_PATH="foobarz" #same from above
KERNEL_TESTING="000" #read above
UNTARED_KERNEL=
KERNEL_SOURCE_FILE=

####
# Functions

download_kernel_xml() {
	KERNEL_TEMP_XML="kernel_version.xml"
	wget -q -O ${KERNEL_TEMP_XML} ${KERNEL_SITE_FEEDS}
}

list_kernel_version() {
	download_kernel_xml
	LIST=`grep "strong&gt;" ${KERNEL_TEMP_XML} | awk -F "strong" '{print $2}' | sed "s/\&lt\;\///g; s/\&gt\;//g"`
	echo "Available Kernel versions for download"
	echo
	for kernel in ${LIST[@]}
	do
		echo -e "\t $kernel"
	done
	echo
	rm ${KERNEL_TEMP_XML}
}

####
# input 
# kernel_version
parse_kernel() {

	download_kernel_xml

	#parse xml by the tag 'Source' and version given
	KERNEL_URL=`grep ${KERNEL_STABLE_VERSION} ${KERNEL_TEMP_XML} | grep "Source" | sed  's/.*href=\"//g' | sed 's/\"&gt.*//g'`
	#echo $KERNEL_URL
	if [ -z ${KERNEL_URL} ]
	then
		echo "Something wrong has happened!"
		echo "I can not find the URL download for Kernel: ${KERNEL_STABLE_VERSION}"
		exit -1
	else
		TO_DOWNLOAD[0]=${KERNEL_URL}
	fi

	#parse xml by the tag 'PGP Signature' and version given
	KERNEL_SIG=`grep ${KERNEL_STABLE_VERSION} ${KERNEL_TEMP_XML} | grep "PGP Signature" | sed  's/.*href=\"//g' | sed 's/\"&gt.*//g'`
	if [ -z ${KERNEL_SIG} ]
	then
		echo "Something wrong has happened!"
		echo "I can not find the URL download for Kernel Sig: ${KERNEL_SIG}"
		exit -1
	else
		TO_DOWNLOAD[1]=${KERNEL_SIG}
	fi

	rm ${KERNEL_TEMP_XML}
}

#######
## Get GrSecurity URL from XML url
parse_grsecurity() {
	#TODO have to implement Auth Basic
	GRSEC_TEMP_XML="grsec_version.xml"
	wget -q -O ${GRSEC_TEMP_XML} ${GRSEC_SITE_FEEDS}

	#cat grsec.tmp | grep 3.2.68 | grep link | sed "s/<.[a-z]*>//g"
	#parse xml by the tag 'link' and version given
	GRSEC_URL=`grep ${KERNEL_STABLE_VERSION} ${GRSEC_TEMP_XML} | grep "link" | sed "s/<.[a-z]*>//g"`
	if [ -z ${GRSEC_URL} ]
	then
		echo "Something wrong has happened!"
		echo "I can not find the URL download for grsec patch: ${GRSEC_URL}"
		exit -1
	else
		TO_DOWNLOAD[2]=${GRSEC_URL}
	fi

	GRSEC_SIG="${GRSEC_URL}.sig"
	if [ -z ${GRSEC_URL} ]
	then
		echo "Something wrong has happened!"
		echo "I can not find the URL download for grsec sig: ${GRSEC_SIG}"
		exit -1
	else
		TO_DOWNLOAD[3]=${GRSEC_SIG}
	fi

	rm $GRSEC_TEMP_XML
}

#######
## Get GrSecurity TESING URL from XML url
parse_grtesting() {
	GRSEC_TEMP_XML="grsec_version.xml"
	wget -q -O ${GRSEC_TEMP_XML} ${GRSEC_TESTING_FEEDS}

	#cat grsec.tmp | grep 3.2.68 | grep link | sed "s/<.[a-z]*>//g"
	#parse xml by the tag 'link' and version given
	GRSEC_URL=`grep test ${GRSEC_TEMP_XML} | grep "link" | grep "patch" | sed "s/<.[a-z]*>//g"`

	if [ -z ${GRSEC_URL} ]
	then
		echo "Something wrong has happened!"
		echo "I can not find the URL download for grsec patch: ${GRSEC_URL}"
		exit -1
	else
		TO_DOWNLOAD[2]=${GRSEC_URL}
	fi

	GRSEC_SIG="${GRSEC_URL}.sig"
	if [ -z ${GRSEC_URL} ]
	then
		echo "Something wrong has happened!"
		echo "I can not find the URL download for grsec sig: ${GRSEC_SIG}"
		exit -1
	else
		TO_DOWNLOAD[3]=${GRSEC_SIG}
	fi

	rm $GRSEC_TEMP_XML
}

download_url() {
	for URL in ${TO_DOWNLOAD[@]}
	do
		wget -c ${URL}
	done
}

unpack_kernel() {
	KERNEL_SOURCE_FILE=`basename ${TO_DOWNLOAD[0]}`
	RESULT="-1"

	if [ -e ${KERNEL_SOURCE_FILE} ]
	then
		${XZ_UNTAR} ${KERNEL_SOURCE_FILE}
		#${UNTAR} ${KERNEL_SOURCE_FILE}
		#ls "${KERNEL_SOURCE_FILE}" > /dev/null
		RESULT=$?
	fi
	echo "$RESULT"
}

check_kernel_sig() {
	KERNEL_SIG_FILE=`basename ${TO_DOWNLOAD[1]}`
	RESULT="-1"

	echo "--> Checking Kernel key"
	gpg --verify ${KERNEL_SIG_FILE} &> /dev/null
	RESULT=$?

	if [ ${RESULT} -ne 0 ]; then
		echo "I got an error verifying GPG key for ${KERNEL_SIG_FILE}"
		echo "Do you have Kernel's key: $GREG_KEY?"
		echo "You should import Kernel's key with: gpg --recv-key ${GREG_KEY}"
	fi
}

check_grsec_sig() {
	GRSEC_SIG_FILE=`basename ${TO_DOWNLOAD[3]}`
	RESULT="-1"

	echo "--> Checking GrSecurity key"
	gpg --verify ${GRSEC_SIG_FILE} &> /dev/null
	RESULT=$?

	if [ ${RESULT} -ne 0 ]; then
		echo "I got an error verifying GPG key for ${GRSEC_SIG_FILE}"
		echo "Do you have GrSecurity's key: $GRSC_KEY"
		echo "You should import Kernel's key with: gpg --recv-key ${GRSC_KEY}"
	fi
	#TODO  Download otherwise
}

copy_kernel_config() {

	#TODO for some reason I can not see this KERNEL_SOURCE_FILE content here
	# I need to get content from TO_DOWNLOAD array
	# I have no idea what it`s wrong
	KERNEL_SOURCE_FILE=`basename ${TO_DOWNLOAD[0]}`

	UNTARED_KERNEL=`echo ${KERNEL_SOURCE_FILE} | sed "s/\.tar\.xz//g"`
	KERNEL_SOURCE_FILE=`echo ${KERNEL_SOURCE_FILE} | sed "s/\.xz//g"`
	${UNTAR} ${KERNEL_SOURCE_FILE} #kernel sig check goes over .tar file
	if [ -f ${CONFIG_PATH} ];then
		#TODO check for local path where unpacked kernel is
		cp ${CONFIG_PATH} "${UNTARED_KERNEL}/.config"
	fi
}

apply_patch_config() {
	echo $KERNEL_SOURCE_FILE
	GRSEC_PATCH=`basename ${TO_DOWNLOAD[2]}`

	if [ -d $UNTARED_KERNEL ];then
		cd ${UNTARED_KERNEL}
		make oldconfig
		patch -p1 < "../${GRSEC_PATCH}"
	fi
}

usage() {
	echo ""
	echo -e "\t-c\t config file path (/boot/config-3.2.68)"
	echo -e "\t-k\t kernel version to download (only numbers 3.2.69)"
	echo -e "\t-l\t list all Kernel versions available for download"
	echo -e "\t-t\t download testing patch according new GrSecurity Policy"
	#echo -e "\t-B\t generate .deb kernel files (TODO)"
	echo ""
}

### main code
if [ $# -lt "1" ];
then
	usage
	exit -1
fi


# -c config file
# -k kernel to download
# -t download grSec testing
# -l list all kernels available to download
# -B build all  stuff TODO
while getopts "c:k:tl" opt
do
	case ${opt} in
		c)
			CONFIG_PATH=${OPTARG}
			;;
		k)
			KERNEL_STABLE_VERSION=${OPTARG}
			;;
		t)

			KERNEL_TESTING="001"
			;;
		l)
			list_kernel_version
			exit 0
			;;

		\?)
			echo "Invalid option"
			;;
	esac
done

if [ ${KERNEL_STABLE_VERSION} == "000" ]; then
	echo "you should provide the kernel version do download"
	exit -1
fi
if [ ${CONFIG_PATH} == "foobarz" ]; then
	echo "you should provide the kernel's config file path"
	exit -1
fi

#KERNEL_STABLE_VERSION=${1}
parse_kernel $KERNEL_STABLE_VERSION

if [ $KERNEL_TESTING == "001" ]
then
	parse_grtesting
else
	parse_grsecurity $KERNEL_STABLE_VERSION
fi

download_url

RESULT=$(unpack_kernel)
if [ ${RESULT} -eq "0" ]
then
	check_kernel_sig
	check_grsec_sig
fi

copy_kernel_config
apply_patch_config

echo
echo "**** Warning"
echo "You should now run  'make menuconfig' inside of $UNTARED_KERNEL and select and save"
echo "Security Options -> GrSecurity -> Configuration Method - Automatic"
echo
echo "run the following command to generate .deb kernel file"
echo "$ fakeroot make deb-pkg"
echo
